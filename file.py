"""
This function writes dict structs to csv using only csv module
"""

''' 
def write_to_csv(filename, headers, dict1):
    if not dict1:
        return False
    try:
        with open(filename, 'w') as csvfile:
            if headers:
                csvfile.write("%s\n" % headers)
            for data in dict1.keys():
                csvfile.write("%s,%s,%s\n" % (data, dict1[data][0], dict1[data][1]))
    except IOError:
        print("Could not save csv file... I/O error!")

    return True
'''


def write_to_csv(filename, headers, dict1):
    if not dict1:
        return False
    try:
        with open(filename, 'w') as csvfile:
            if headers:
                csvfile.write("%s\n" % headers)
            for data in dict1.keys():
                csvfile.write("%s,%s\n" % (data, dict1[data]))
    except IOError:
        print("Could not save csv file... I/O error!")

    return True
