#!/usr/bin/env bash

MASA_WORKFLOW_BUCKET='masa-workflow'
MASA_INPUTS_BUCKET_PREFIX="${MASA_WORKFLOW_BUCKET}/inputs"
MASA_OUTPUTS_BUCKET_PREFIX="${MASA_WORKFLOW_BUCKET}/outputs2"

LOCAL_BASEDIR=$(pwd)
LOCAL_INPUTS_DIR_PATH="${LOCAL_BASEDIR}/inputs"
LOCAL_OUTPUTS_DIR_PATH="${LOCAL_BASEDIR}/outputs"

INSTANCE_TYPE=$(curl http://169.254.169.254/latest/meta-data/instance-type)
DISTRO='ubuntu'
COMPILER='gcc'
DEPENDENCIES=(python3-pandas python3-memory-profiler)
COMPLETE_OUTPUTS_BUCKET_PATH="${MASA_OUTPUTS_BUCKET_PREFIX}/${INSTANCE_TYPE}/${DISTRO}/${COMPILER}/gustafson/bigger-sequences"

TELEGRAM_TOKEN='1489921474:AAEKRAIGRG5E9vtCD9EtaSaC3qdnV_NwB80'
TELEGRAM_CHAT='142684352'

DATASET='50'
SEQUENCE_SIZES_LIST=('30k' '120k')
NUMBER_OF_PARALLEL_PROCESSES_LIST=(1 24 48 60 72 96)
NUMBER_OF_THREADS_PER_PROCESS_LIST=(1)
NUMBER_OF_ROUNDS=2

# Pre execution checks
# LOCAL basedir
[[ -d "${LOCAL_BASEDIR:?}" ]] && cd "${LOCAL_BASEDIR}" || exit
# LOCAL Inputs dir
[[ -d "${LOCAL_INPUTS_DIR_PATH:?}" ]] || mkdir -p "${LOCAL_INPUTS_DIR_PATH}"
# LOCAL Outputs
[[ -d "${LOCAL_OUTPUTS_DIR_PATH:?}" ]] || mkdir -p "${LOCAL_OUTPUTS_DIR_PATH}"

function install_dependencies() {
  sudo apt install -y "${DEPENDENCIES[@]}"
}

# get_inputs_from_s3 function downloads input files from an S3 bucket indicated by ${MASA_INPUTS_BUCKET_PREFIX} variable,
# into a local directory which path is indicate by ${LOCAL_INPUTS_DIR_PATH} variable
function get_inputs_from_s3() {
  [[ -d "${LOCAL_INPUTS_DIR_PATH}" ]] && rm -rf "${LOCAL_INPUTS_DIR_PATH}"
  aws s3 sync "s3://${MASA_INPUTS_BUCKET_PREFIX}/${DATASET}/" "${LOCAL_INPUTS_DIR_PATH}/"
}

# run_masa function runs 'masa-workflow.py' with multiple times passing arguments
function run_masa() {
  for seq_size in "${SEQUENCE_SIZES_LIST[@]}"; do
    aws s3 cp - "s3://${COMPLETE_OUTPUTS_BUCKET_PATH}/${seq_size}/cpuinfo" < /proc/cpuinfo
    free -m | aws s3 cp - "s3://${COMPLETE_OUTPUTS_BUCKET_PATH}/${seq_size}/meminfo"
    [[ -d "${LOCAL_OUTPUTS_DIR_PATH:?}/${seq_size}" ]] || mkdir -p "${LOCAL_OUTPUTS_DIR_PATH}/${seq_size}"
    for nworkflows in "${NUMBER_OF_PARALLEL_PROCESSES_LIST[@]}"; do
      for nthreads in "${NUMBER_OF_THREADS_PER_PROCESS_LIST[@]}"; do
        for round in $(seq "${NUMBER_OF_ROUNDS}"); do
          python3 masa-workflow.py --ref-seq "ref-wuhan-${seq_size}.fa" \
                                 --output-dir "${LOCAL_OUTPUTS_DIR_PATH}/${seq_size}/${nworkflows}/${round}/" \
                                 --input-dir "${LOCAL_INPUTS_DIR_PATH}/${seq_size}/" \
                                 --nworkflows "${nworkflows}" \
                                 --threads "${nthreads}" \
                                 --options='--verbose=0' >> "${LOCAL_OUTPUTS_DIR_PATH:?}/${seq_size}/masa-${nworkflows}w-$(date +%F).log"
        done
        # For Amdahl, the location should include the number of threads!!!
        aws s3 sync "${LOCAL_OUTPUTS_DIR_PATH:?}/${seq_size}/" "s3://${COMPLETE_OUTPUTS_BUCKET_PATH}/${seq_size}/"
        # aws s3 cp "${LOCAL_OUTPUTS_DIR_PATH:?}/${seq_size}/masa-${nworkflows}w-$(date +%F).log" "s3://${COMPLETE_OUTPUTS_BUCKET_PATH}/${seq_size}/masa-${nworkflows}w-$(date +%F).log"
        rm -rf "${LOCAL_OUTPUTS_DIR_PATH:?}/${seq_size}/*"
      done
    done
  done
}

function send_report_to_telegram() {
  chmod +x ./tools/telegram
  ./tools/telegram -t "${TELEGRAM_TOKEN}" -c "${TELEGRAM_CHAT}" "$1"
}

send_report_to_telegram "MASA started running on ${INSTANCE_TYPE} at $(date +%R) of $(date +%F)."
install_dependencies
get_inputs_from_s3
run_masa
send_report_to_telegram "MASA finished running on ${INSTANCE_TYPE} at $(date +%R) of $(date +%F). The results are in in the bucket ${COMPLETE_OUTPUTS_BUCKET_PATH}"
sudo shutdown -h now

exit 0
