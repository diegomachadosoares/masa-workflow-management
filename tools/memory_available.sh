#!/usr/bin/env bash

MASA_WORKFLOW_BUCKET='masa-workflow'

MASA_OUTPUTS_BUCKET="${MASA_WORKFLOW_BUCKET}/outputs2"
INSTANCE_TYPE=$(curl http://169.254.169.254/latest/meta-data/instance-type)
DISTRO='ubuntu'
OUTPUT_BUCKET="${MASA_OUTPUTS_BUCKET}/memory/${INSTANCE_TYPE}/${DISTRO}"

LOCAL_BASEDIR='/tmp/memory_available'
LOCAL_OUTPUT_DIR="${LOCAL_BASEDIR}"

[[ -d "${LOCAL_BASEDIR}" ]] && cd "${LOCAL_BASEDIR}" || mkdir -p "${LOCAL_BASEDIR}"

function run_test() {
  for i in {1..10};
  do
    free -m > "${LOCAL_BASEDIR}/${i}.free"
    sleep 60
  done
}

function put_outputs_to_s3() {
  aws s3 sync "${LOCAL_OUTPUT_DIR}/" "s3://${OUTPUT_BUCKET}/memory_available/"
  aws s3 cp /proc/cpuinfo "s3://${OUTPUT_BUCKET}/memory_available/cpuinfo-$(date +%F)"
}

run_test
put_outputs_to_s3
sudo shutdown -h now

exit 0
