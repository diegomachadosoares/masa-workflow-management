#!/usr/bin/env bash

MASA_WORKFLOW_BUCKET='masa-workflow'

MASA_INPUTS_BUCKET="${MASA_WORKFLOW_BUCKET}/inputs"
MASA_OUTPUTS_BUCKET="${MASA_WORKFLOW_BUCKET}/outputs2"

LOCAL_BASEDIR=$(pwd)
LOCAL_INPUTS_DIR="${LOCAL_BASEDIR}/inputs"
LOCAL_OUTPUT_DIR="${LOCAL_BASEDIR}"

INSTANCE_TYPE=$(curl http://169.254.169.254/latest/meta-data/instance-type)
DISTRO='ubuntu'
COMPILER='gcc'
OUTPUT_BUCKET="${MASA_OUTPUTS_BUCKET}/${INSTANCE_TYPE}/${DISTRO}/${COMPILER}/bigger-sequences/pin/balanced"

TELEGRAM_TOKEN='1489921474:AAEKRAIGRG5E9vtCD9EtaSaC3qdnV_NwB80'
TELEGRAM_CHAT='142684352'

[[ -d "${LOCAL_BASEDIR}" ]] && cd "${LOCAL_BASEDIR}" || exit

function get_inputs_from_s3() {
  [[ -d "${LOCAL_INPUTS_DIR}" ]] && rm -rf "${LOCAL_INPUTS_DIR}"
  aws s3 sync "s3://${MASA_INPUTS_BUCKET}/sars-cov2-bigger-seq/" "${LOCAL_INPUTS_DIR}/"
}

function run_masa() {
  for nthreads in {1,24,48,72,96}; do
    for i in {0..2}; do
      GOMP_CPU_AFFINITY='0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47' python3 masa-workflow.py --ref-seq "ref-wuhan-480k.fa" --seq "ref-MT012098.1-480k.fa" --output-dir "./outputs-480k-pin/${i}/" --nworkflows "1" --threads "48" --options='--verbose=0'
    done
  done
}

function put_outputs_to_s3() {
  aws s3 sync "${LOCAL_OUTPUT_DIR}/outputs-480k-pin/" "s3://${OUTPUT_BUCKET}/outputs-480k-pin/"
  cat /proc/cpuinfo >/tmp/cpuinfo
  aws s3 cp /tmp/cpuinfo "s3://${OUTPUT_BUCKET}/output-480k-pin/cpuinfo"
}

function send_report_to_telegram() {
  chmod +x ./tools/telegram
  ./tools/telegram -t "${TELEGRAM_TOKEN}" -c "${TELEGRAM_CHAT}" "$1"
}

#send_report_to_telegram "MASA started running on ${INSTANCE_TYPE} at $(date +%R) of $(date +%F)."
#get_inputs_from_s3
run_masa
put_outputs_to_s3
#send_report_to_telegram "MASA finished running on ${INSTANCE_TYPE} at $(date +%R) of $(date +%F). The results are in in the bucket ${OUTPUT_BUCKET}"

exit 0
