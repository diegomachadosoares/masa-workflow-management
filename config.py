import logging
import os
import pathlib
import shutil
import sys
import uuid

import yaml

import aws

BASE_DIR = pathlib.Path(__file__).resolve(strict=True).parent
ENV = os.environ

'''
configure_workflows function configures the workflow with parameters either from a config file or the CLI
'''


def configure_workflows(cli_args):
    if cli_args.config is not None and os.path.isfile(str(cli_args.config)):
        loader = yaml.SafeLoader
        try:
            with open(cli_args.config, 'r') as config:
                config_dict = yaml.load(config, loader)
        except IOError:
            print("Error: File not found.")
            return 0

        if 'reference_sequence' in config_dict:
            ref_seq = config_dict['reference_sequence']
        else:
            # TODO - Here should change to all vs all comparison mode, not default to wuhan...
            ref_seq = 'ref-wuhan.fa'

        num_threads = config_dict['max_threads_per_comparison']
        nworkflows = config_dict['number_of_workflows']
        if 'input_path' in config_dict:
            input_path = config_dict['input_path']
        else:
            input_path = '/'
        if config_dict['input_type'] == 'local' or config_dict['input_type'] == 's3':
            if config_dict['input_type'] == 'local':
                input_path = pathlib.Path(BASE_DIR).joinpath(config_dict['input_path'])
            elif config_dict['input_type'] == 's3':
                if 'aws_region' not in config_dict or 'input_bucket' not in config_dict:
                    raise OSError("You must specify AWS region and input_bucket when input_type is s3...")
                else:
                    logging.info(input_path)
                    if aws.get_inputs_from_s3(config_dict['aws_region'], config_dict['input_bucket'], str(input_path),
                                              './inputs') != 0:
                        raise OSError("Could not get inputs from S3! Verify the parameters and try again...")
                    input_path = pathlib.Path(BASE_DIR).joinpath('./inputs')

        else:
            raise OSError('Not implemented... only local/s3 inputs!')
        # Find out if output is set to local or S3...
        if config_dict['output_type'] == 'local' or config_dict['output_type'] == 's3':
            if config_dict['output_type'] == 'local':
                output_path = pathlib.Path(BASE_DIR).joinpath(config_dict['output_path'])
            else:
                output_path = pathlib.Path(BASE_DIR).joinpath('./outputs')
        else:
            raise OSError('Not implemented yet... only local/s3 outputs!')

        all_sequences = []
        if 'sequence' in config_dict:
            all_sequences = config_dict['sequences']
            for seq in all_sequences:
                try:
                    os.stat(pathlib.Path(BASE_DIR).joinpath('./inputs').joinpath(seq))
                except OSError:
                    print("Input " + seq + " not found in " + str('./inputs') + " exiting...")
                    exit()
        else:
            try:
                all_sequences = os.listdir('./inputs')
                if ref_seq in all_sequences:
                    all_sequences.remove(ref_seq)
            except OSError:
                print("Can't open inputs directory " + os.getcwd())
                exit()

        if 'binary' in config_dict:
            masa_bin = config_dict['binary']
        else:
            masa_bin = shutil.which("masa-openmp")

        if 'masa_options' in config_dict:
            masa_options = "".join(config_dict['masa_options'])
        else:
            masa_options = None

    else:
        if cli_args.reference is not None:
            ref_seq = cli_args.reference
        else:
            # TODO - Here should change to all vs all comparison mode, not default to wuhan...
            ref_seq = 'ref-wuhan.fa'

        num_threads = cli_args.max_threads
        nworkflows = cli_args.workflows
        input_path = pathlib.Path(BASE_DIR).joinpath(cli_args.input_path)
        output_path = pathlib.Path(BASE_DIR).joinpath(cli_args.output_path)

        all_sequences = []
        if cli_args.sequences is not None:
            all_sequences = cli_args.sequences
            for seq in all_sequences:
                try:
                    os.stat(pathlib.Path(BASE_DIR).joinpath(input_path).joinpath(seq))
                except OSError:
                    print("Input " + seq + " not found in " + str(input_path) + " exiting...")
                    sys.exit()
        else:
            try:
                all_sequences = os.listdir(input_path)
                if ref_seq in all_sequences:
                    all_sequences.remove(ref_seq)
            except OSError:
                print("Can't open inputs directory " + os.getcwd())

        if cli_args.binary is not None:
            masa_bin = cli_args.binary
        else:
            masa_bin = shutil.which("masa-openmp")

        if cli_args.options is not None:
            masa_options = "".join(cli_args.options)
        else:
            masa_options = None

    workflows = []

    for i in range(nworkflows):
        workflow_id = uuid.uuid4()
        workflow_config = {'masa_bin': masa_bin,
                           'masa_opts': masa_options,
                           'env': ENV,
                           'input_path': input_path,
                           'output_path': pathlib.Path(
                               pathlib.Path(output_path).joinpath(str(nworkflows) + "w")).joinpath(
                               str(workflow_id)),
                           'compare_input': ref_seq,
                           'num_threads': num_threads,
                           'concurrency': 1,
                           'sequences': all_sequences,
                           'workflow_id': workflow_id}
        workflows.append(workflow_config)
    return workflows


