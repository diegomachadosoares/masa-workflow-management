Masa Workflow Manager
=========

[MASA](https://github.com/edanssandes/MASA-OpenMP) workflow manager is a workflow management tool
designed to help in the management of the parallel and concurrent execution of multiple 
MASA-OpenMP comparisons workflows as part of my Master's thesis in Parallel and Distributed Computing.

Each MASA workflow is composed of one or more conparisons either with one reference sequence (one to many)
or among other sequences (many to many).

MASA workflow manager is under active development and in its early stages, therefore things are expected to change
and sometimes break between versions.

It is important to note that it is a part of a larger concept (my master's thesis) composed of multiple pieces.
Our ultimate goal is to have a general workflow management tool that helps to efficiently execute workflows in the public cloud.

It is also worth noting that there are no dependencies between comparisons in a MASA's workflows,
therefore MASA's workflows can be seen essentially as a bag of tasks type of workflow.

Dependencies
------------

This project controls its dependecies using a requirements.txt file.\
It should be easy to install all dependencies at once with a simple command using
python's package managementl tool `pip`, as follows.

     $ python -m pip install -r requirements.txt

See the [requirements.txt](requirements.txt) file to a list of all dependecies.

Configuration
--------------

There are two ways to configure MASA workflow manager's execution, by passing CLI parameters or with a configuration file.

### CLI parameters

See [masa-workflow.py](masa-workflow.py) file for all configuration options...

### Config file

See [masa-workflow.py](masa-workflow.py) file for all configuration options...

    Important! If a config file is passed all other parameters are ignored.

Examples
----------------

CLI parameters execution example:

     $ python3 masa-workflow.py --reference-sequence ref-wuhan.fa --output-type local --output-path ./example_data/outputs --input-type local --input-path ./example_data/inputs --max-threads 1 --nworkflows 1 --options='--verbose=0'

Executing multiple iterations with multiple number of workflows in parallel passing parameters through the CLI:

     $ for iteration in {0,1}; do for nworkflows in {1,2,4}; do python3 masa-workflow.py --ref-seq ref-wuhan.fa --output-type 'local' --output-path ./example_data/outputs/${iteration} --input-type 'local' --input-path ./example_data/inputs --threads 1 --nworkflows ${nworkflows} --options='--verbose=0'; done; done

Config file execution example:

    $ python3 masa-workflow.py --config config.yml

See [config.yml](config.yml) for an example configuration

Development and Testing
-----------------------

This project uses [vagrant](https://vagrantup.com) for development environment and testing, see the [docs](https://learn.hashicorp.com/tutorials/vagrant/getting-started-index?in=vagrant/getting-started) for more information.

### TL;DR
- Install vagrant
- Install virtualbox
- Enter the project directory and invoke the `vagrant up` command.
- Execute `vagrant ssh` to connect to the virtual machine created.
- After finish, execute `vagrant destroy` command to remove the virtual machine created.

License
-------

This project is licensed under the MIT license.\
See the [LICENSE](LICENSE) file for more details.

Author Information
------------------

Created by Diego M. Soares
